<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 23.11.17
 * Time: 23:02
 */

class Parser
{
    protected $siteName = 'https://tiu.ru';
    protected $proxy = [];
    protected $productLinks = [];
    protected $blackList = [];

    public function __construct()
    {
        $file = fopen('proxy.csv', 'r');
        while (!feof($file)) {
            $server = fgetcsv($file);
            $this->proxy[$server[0]] = $server[1];
        }

        fclose($file);
        $this->getSitemapXml();
        $this->getContacts();

    }

    protected function getSitemapXml()
    {
        $success = false;
        while ($success == false) {
            $out = $this->getFile($this->siteName . '/sitemap.xml');

            if (!strpos($out, '<h1>Защита от роботов</h1>')) {
                $success = true;
            }
            try {
                $resources = new SimpleXMLElement($out);
            } catch (\Exception $e) {
                $success = false;
            }
        }

        foreach ($resources as $resource) {

            if (strripos($resource->loc, 'sitemap_categories-')) {
                $this->getCategoriesList($resource->loc);
            }
        }
    }

    protected function getCategoriesList($url)
    {
        $success = false;
        while ($success == false) {
            $html = $this->getFile($url);

            if (!strpos($html, '<h1>Защита от роботов</h1>')) {
                $success = true;
            }

            try {
                $categories = new SimpleXMLElement($html);
            } catch (\Exception $e) {
                $success = false;
            }
        }

        foreach ($categories as $category) {
            $this->getProducts($category->loc);
        }


//        preg_match_all('/data-page="(.*?)"/', $html, $matches);
//        if (isset($matches[1])) {
//            $pages = (int)max($matches[1]);
//            for ($i = 2; $i <= $pages; $i++) {
//                $this->getProducts(str_replace(".html", ";$i.html", $link), true);
//            }
//        }

    }


    protected function getFile($url)
    {
        $start = microtime(true);
        $proxyIp = array_rand($this->proxy);
        $proxyPort = $this->proxy[$proxyIp];


        $agent = 'Mozilla/5.0 (Linux; U; Android 4.0; en-us; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, $agent);
        curl_setopt($curl, CURLOPT_PROXY, $proxyIp); // replace
        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS4);
        curl_setopt($curl, CURLOPT_PROXYPORT, $proxyPort); // replace '2211' with Proxy server's port.

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 2);
        curl_setopt($curl, CURLOPT_REFERER, 'https://www.google.com.ua');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//        $headers = [
//            'user-agent:Mozilla/5.1 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',
//            'cookie:ccc=WhyL-rKWeJ6Q6bogStxx_8DbB0qaEi0QiMVfHwjfKvW2XfKVIBzaIysluLQmblkEEVwQiA0DbGWOZRaeGh0dyw==; cid=144284001939415927378953434552008373954; user_tracker=7e99dd086bc5d7a6e97443206a243673d19c0adf|178.150.120.158|2017-11-25; region_id=194; auth=8a2ddf44e67820db0622e39d131cffa94e6b9587; unauth=ee4c4fcc99d9f5a7cee64a30665235925ebc9b02; ext_referer=aHR0cHM6Ly9wcm9tLnVhLw==; prom=c06263de0d9da88f8cc7f46ec5ae77525a7d8311a798c22643bd47debc0e188eb983d1da; _ga=GA1.2.c-LCTpE0kkJsEJrQbKZmPibnwh; _gid=GA1.2.144976289.1511561083; sc=0EC819E2-ED3B-3543-FB49-A7CF8250302B; csrf_token=7f28ca940876410cb71c53b4e2c5e32e',
//
//        ];
//        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $out = curl_exec($curl);
        curl_close($curl);
        if (empty($out)) {
            unset($this->proxy[$proxyIp]);
        }

        $end = microtime(true);

        $resultTime = $end - $start;
        if ($resultTime > 3) {
            unset($this->proxy[$proxyIp]);
        }

        if (strpos($out, '<h1>Защита от роботов</h1>')) {
            unset($this->proxy[$proxyIp]);
        }
        echo 'Servers: ' . count($this->proxy) . "\n";
        return $out;
    }


    protected function getContacts()
    {

        $dbh = new PDO('mysql:host=localhost;dbname=mydb;charset=utf8', 'root', 'root');
        $companies = $dbh->query("select * from company_ru")->fetchAll();
        $count = 0;
        foreach ($companies as $company) {
            $count++;
            $company = $company[0];
            echo 'Start parsing: ' . $company . "\n";

            $success = false;
            while ($success == false) {
                $html = $this->getFile($this->siteName . '/company_info/contacts/' . $company);

                preg_match('/<h1 class="x-title">(.*?)<\/h1>/', $html, $matches);
                if (isset($matches[1])) {
                    $shop = $matches[1];
                }

                preg_match_all('/<span data-qaid=\'phone\'>(.*?)<\/span>/', $html, $matches);
                if (isset($matches[1])) {
                    $tel = implode(',', $matches[1]);
                }


                preg_match_all('/<td class="x-company-contacts__cell" data-qaid=\'contact_person\'>(.*?)<\/td>/', $html, $matches);
                if (isset($matches[1])) {
                    $name = implode(',', $matches[1]);
                }

                preg_match_all('/<td class="x-company-contacts__cell" data-qaid=\'address\'>(.*?)<\/td>/', $html, $matches);
                if (isset($matches[1])) {
                    $address = implode(',', $matches[1]);
                }

                if (empty($html)) {
                    continue;
                }

                if (strpos($html, 'svg-page-404')) {
                    echo 'Invalid shop' . "\n";
                    break;
                }

                if (empty($tel)) {
                    echo 'Invalid phone' . "\n";
                    continue;
                }

                $left = count($companies) - $count;
                echo 'Contact saved: ' . $company . '. Count: ' . $count . '. Left: ' . $left . "\n";
                $query = $dbh->prepare("insert into users_ru (shop, name, phone, site_id, address) values ('$shop', '$name', '$tel', '$company' , '$address');");
                $query->execute();
                $success = true;
            }
        }
    }

    protected $getAnabled = false;

    public function getProducts($link, $onFirstPage = false)
    {
        $success = false;

        if (strpos($link, 'wholesale')) {
            return;
        }
        if (strpos($link, 'discount')) {
            return;
        }

        $dbh = new PDO('mysql:host=localhost;dbname=mydb;charset=utf8', 'root', 'root');
        $query = $dbh->prepare("insert into company_ru (id) values (:id);");
//        if ($link == "t12313est") {
//            $this->getAnabled = true;
//        }
//
//
//        if (!$this->getAnabled) {
//            echo 'SKIP' . "\n";
//            return;
//        }
        while ($success == false) {


            $html = $this->getFile($link);
            if (empty($html)) {
                continue;
            }

            if (strpos($html, '<h1>Защита от роботов</h1>')) {
                continue;
            }

            if (strpos($html, 'По запросу ничего не найдено')) {
                return;
            }

            echo 'Parse :' . $link . "\n";
            preg_match_all('/"ec_company_id": "(.*?)"/', $html, $matches);
            if (isset($matches[1])) {
                $comps = $matches[1];
//                $comps = "('" . implode("'),('", $comps) . "')";
                foreach ($comps as $comp) {
                    $query->bindValue(':id', $comp, PDO::PARAM_STR);
                    $query->execute();
                }
            }

//            preg_match('/"nextPageUrl": "(.*?)"/', $html, $matches);
//            if (isset($matches[1])) {
//                $next = $matches[1];
//                if ($this->siteName . $next != $link) {
//                    $this->getProducts($this->siteName . $next, true);
//                }
//            }


//            if (isset($matches[1])) {
//                var_dump($matches[1]);
//                exit();
//                $pages = (int)max($matches[1]);
//                for ($i = 2; $i <= $pages; $i++) {
//                    while ($success == false) {
//                        $html = $this->getFile(str_replace(".html", ";$i.html", $link));
//
//                        if (empty($html)) {
//                            continue;
//                        }
//
//                        if (strpos($html, '<h1>Защита от роботов</h1>')) {
//                            continue;
//                        }
//
//                        preg_match_all('/"ec_company_id": "(.*?)"/', $html, $matches);
//                        if (isset($matches[1])) {
//                            $comps = $matches[1];
//                            echo 'next pages scanned ' . $i;
//                        }
//                        $success = true;
//                    }
//                }
//            }


            $success = true;

        }
        $dbh = null;
    }
}